package de.tuberlin.sese.swtpp.gameserver.model.deathstacks;

import de.tuberlin.sese.swtpp.gameserver.model.Game;
import de.tuberlin.sese.swtpp.gameserver.model.Move;
import de.tuberlin.sese.swtpp.gameserver.model.Player;
// TODO: more imports allowed

public class DeathStacksGame extends Game {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3053592017994489843L;
	/************************
	 * member
	 ***********************/
	
	// just for better comprehensibility of the code: assign blue and red player
	private Player bluePlayer;
	private Player redPlayer;

	// TODO: internal representation of the game state 
	private String state;
	private String[][] gameBoard;
	/************************
	 * constructors
	 ***********************/
	
	public DeathStacksGame() throws Exception{
		super();
		initGameBoard();
	}

	public String getType() {
		return "deathstacks";
	}
	
	/*******************************************
	 * Game class functions already implemented
	 ******************************************/
	
	@Override
	public boolean addPlayer(Player player) {
		if (!started) {
			players.add(player);
			
			if (players.size() == 2) {
				started = true;
				this.redPlayer = players.get(0);
				this.bluePlayer = players.get(1);
				nextPlayer = this.redPlayer;
			}
			return true;
		}
		
		return false;
	}

	@Override
	public String getStatus() {
		if (error) return "Error";
		if (!started) return "Wait";
		if (!finished) return "Started";
		if (surrendered) return "Surrendered";
		if (draw) return "Draw";
		
		return "Finished";
	}
	
	@Override
	public String gameInfo() {
		String gameInfo = "";
		
		if(started) {
			if(blueGaveUp()) gameInfo = "blue gave up";
			else if(redGaveUp()) gameInfo = "red gave up";
			else if(didRedDraw() && !didBlueDraw()) gameInfo = "red called draw";
			else if(!didRedDraw() && didBlueDraw()) gameInfo = "blue called draw";
			else if(draw) gameInfo = "draw game";
			else if(finished)  gameInfo = bluePlayer.isWinner()? "blue won" : "red won";
		}
			
		return gameInfo;
	}	
	
	@Override
	public String nextPlayerString() {
		return isRedNext()? "r" : "b";
	}

	@Override
	public int getMinPlayers() {
		return 2;
	}

	@Override
	public int getMaxPlayers() {
		return 2;
	}
	
	@Override
	public boolean callDraw(Player player) {
		
		// save to status: player wants to call draw 
		if (this.started && ! this.finished) {
			player.requestDraw();
		} else {
			return false; 
		}
	
		// if both agreed on draw:
		// game is over
		if(players.stream().allMatch(p -> p.requestedDraw())) {
			this.finished = true;
			this.draw = true;
			redPlayer.finishGame();
			bluePlayer.finishGame();
		}	
		return true;
	}
	
	@Override
	public boolean giveUp(Player player) {
		if (started && !finished) {
			if (this.redPlayer == player) { 
				redPlayer.surrender();
				bluePlayer.setWinner();
			}
			if (this.bluePlayer == player) {
				bluePlayer.surrender();
				redPlayer.setWinner();
			}
			finished = true;
			surrendered = true;
			redPlayer.finishGame();
			bluePlayer.finishGame();
			
			return true;
		}
		
		return false;
	}

	/*******************************************
	 * Helpful stuff
	 ******************************************/
	
	/**
	 * 
	 * @return True if it's white player's turn
	 */
	public boolean isRedNext() {
		return nextPlayer == redPlayer;
	}
	
	/**
	 * Finish game after regular move (save winner, move game to history etc.)
	 * 
	 * @param player
	 * @return
	 */
	public boolean finish(Player player) {
		// public for tests
		if (started && !finished) {
			player.setWinner();
			finished = true;
			redPlayer.finishGame();
			bluePlayer.finishGame();
			
			return true;
		}
		return false;
	}

	public boolean didRedDraw() {
		return redPlayer.requestedDraw();
	}

	public boolean didBlueDraw() {
		return bluePlayer.requestedDraw();
	}

	public boolean redGaveUp() {
		return redPlayer.surrendered();
	}

	public boolean blueGaveUp() {
		return bluePlayer.surrendered();
	}

	/*******************************************
	 * !!!!!!!!! To be implemented !!!!!!!!!!!!
	 ******************************************/
	
	@Override
	public void setBoard(String state) 
	{
		String[] raw = state.split(" ");
		String stateCode = raw[0];
		String[] rows = stateCode.split("/");
		for(int j = 0 ; j < 6 ; j++)
		{
			for(int i = 0 ; i < 6 ; i++)
			{
				String[] cols = rows[5-i].split(",", -1);
				gameBoard[j][i] = cols[j];
			}
		}
	}
	
	@Override
	public String getBoard() {
		updateState();
		return state;
	}
	
	@Override
	public boolean tryMove(String moveString, Player player) {
		DSMove m = new DSMove(moveString, state, player);
		boolean obeyTooTallRule = isTooTallRule(m);
		if (m.isMoveLegit() && isPlayerCheckerOnTop(m.getFrom()) && obeyTooTallRule)
		{
			updateBoard(m);
			updateState();
			if (checkIfWon(isRedNext()))
			{
				endGame();
			}

			if(isRedNext())
				setNextPlayer(bluePlayer);
			else
				setNextPlayer(redPlayer);
			
			return true;
		}
		return false;
	}	
	
	/**
	 * Tells if player obey too-tall rule.
	 * Too-tall rule means, that player must get rid of stack which height is 5 or more.
	 * @param gameBoard
	 * @return true: rule is obeyed, false: rule is violated
	 */
	public boolean isTooTallRule(DSMove m)
	{
		for(int j = 0 ; j < 6 ; j++)
		{
			for(int i = 0 ; i < 6 ; i++)
			{
				int len = gameBoard[j][i].length();
				if (len > 4)
				{
					if (isRedNext()) 
					{
						if (gameBoard[j][i].charAt(0) == 'b')
							continue;
					}
					else if (gameBoard[j][i].charAt(0) == 'r')
						continue;
					
					if (len - m.getCheckersMovedAmount() > 4) 
						return false;
					
					int fromX = (int)m.getFrom().charAt(0) - 97;
					int fromY = Integer.parseInt(m.getFrom().charAt(1)+"") - 1;
					if (!(fromX == j && fromY == i))
					{
						return false;
					}
				}
					
			}
		}
		return true;
	}
	
	private boolean isPlayerCheckerOnTop(String from)
	{
		int fromCol = (int)from.charAt(0) - 97;
		int fromRow = Integer.parseInt(from.charAt(1)+"");
		if (isRedNext())
		{
			if (gameBoard[fromCol][fromRow - 1].charAt(0) == 'r')
				return true;
			else 
				return false;
		}
		else
		{
			if (gameBoard[fromCol][fromRow - 1].charAt(0) == 'b')
				return true;
			else 
				return false;
		}
	}
	
	public void updateBoard(DSMove m)
	{
		// get cells info (column from 0, row from 1)
		int fromColumnNumber = (int)m.getFrom().charAt(0) - 97;
		int fromRowSegment = Integer.parseInt(m.getFrom().charAt(1)+"");
		
		int toColumnNumber = (int)m.getTo().charAt(0) - 97;
		int toRowSegment = Integer.parseInt(m.getTo().charAt(1)+"");
		
		// update cells on board
		updateCells(fromColumnNumber, fromRowSegment, toColumnNumber, toRowSegment, m.getCheckersMovedAmount());
	}
	
	private void updateCells(int fromCol, int fromSeg, int toCol, int toSeg, int moved)
	{
		// remove
		String toAdd = "";
		for(int i = 0 ; i < moved ; i++)
		{
			toAdd += gameBoard[fromCol][fromSeg-1].charAt(i);
		}
		StringBuilder sb = new StringBuilder(gameBoard[fromCol][fromSeg-1]);
		for(int i = 0 ; i < moved ; i++)
		{
			sb.deleteCharAt(0);
		}
		gameBoard[fromCol][fromSeg-1] = sb.toString();
		//add
		StringBuilder sb1 = new StringBuilder(gameBoard[toCol][toSeg-1]);
		// put on top
		sb1.insert(0, toAdd);
		gameBoard[toCol][toSeg-1] = sb1.toString();
	}
	
	private void updateState()
	{
		state = "";
		for(int j = 5 ; j >= 0 ; j--)
		{
			for(int i = 0 ; i < 6 ; i++)
			{
				state += gameBoard[i][j] + ',';
			}
			state = state.substring(0, state.length()-1);
			state += "/";
		}
		// remove / on end
		state = state.substring(0, state.length()-1);
	}
	
	private boolean checkIfWon(boolean isRed)
	{
		char c = isRed ? 'b':'r';
		for(int j = 0 ; j < 6 ; j++)
		{
			for(int i = 0 ; i < 6 ; i++)
			{
				int len = gameBoard[j][i].length();
				if (len > 0)
				{
					char temp = gameBoard[j][i].charAt(0);
					if (temp == c)
						return false;
				}
					
			}
		}
		return true;
	}
	
	private void endGame()
	{
		if (started && !finished) {
			if (isRedNext())
				redPlayer.setWinner();
			else
				bluePlayer.setWinner();
			finished = true;
			redPlayer.finishGame();
			bluePlayer.finishGame();
			
		}
	}

	private void initGameBoard()
	{
		gameBoard = new String[6][6];
		for(int j = 0 ; j < 6 ; j++)
		{
			for(int i = 0 ; i < 6 ; i++)
			{
				if (j == 0)
					gameBoard[i][j] = "bb";
				else if (j == 5)
					gameBoard[i][j] = "rr";
				else
					gameBoard[i][j] = "";
			}
		}
	}
}
