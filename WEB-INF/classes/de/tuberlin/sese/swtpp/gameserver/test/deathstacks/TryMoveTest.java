package de.tuberlin.sese.swtpp.gameserver.test.deathstacks;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import de.tuberlin.sese.swtpp.gameserver.control.GameController;
import de.tuberlin.sese.swtpp.gameserver.model.Player;
import de.tuberlin.sese.swtpp.gameserver.model.User;
import de.tuberlin.sese.swtpp.gameserver.model.deathstacks.DeathStacksGame;

public class TryMoveTest {

	User user1 = new User("Alice", "alice");
	User user2 = new User("Bob", "bob");
	
	Player redPlayer = null;
	Player bluePlayer = null;
	DeathStacksGame game = null;
	GameController controller;
	
	String gameType ="deathstacks";
	
	@Before
	public void setUp() throws Exception {
		controller = GameController.getInstance();
		controller.clear();
		
		int gameID = controller.startGame(user1, "", gameType);
		
		game = (DeathStacksGame) controller.getGame(gameID);
		redPlayer = game.getPlayer(user1);

	}
	
	public void startGame(String initialBoard, boolean redNext) {
		controller.joinGame(user2, gameType);		
		bluePlayer = game.getPlayer(user2);
		
		game.setBoard(initialBoard);
		game.setNextPlayer(redNext? redPlayer:bluePlayer);
	}
	
	public void assertMove(String move, boolean red, boolean expectedResult) {
		if (red)
			assertEquals(expectedResult, game.tryMove(move, redPlayer));
		else 
			assertEquals(expectedResult,game.tryMove(move, bluePlayer));
	}
	
	public void assertGameState(String expectedBoard, boolean redNext, boolean finished, boolean draw, boolean redWon) {
		String board = game.getBoard();
				
		assertEquals(expectedBoard,board);
		assertEquals(finished, game.isFinished());
		if (!game.isFinished()) {
			assertEquals(redNext, game.isRedNext());
		} else {
			assertEquals(draw, game.isDraw());
			if (!draw) {
				assertEquals(redWon, redPlayer.isWinner());
				assertEquals(!redWon, bluePlayer.isWinner());
			}
		}
	}

	/*******************************************
	 * !!!!!!!!! To be implemented !!!!!!!!!!!!
	 *******************************************/
	
	// simple move means no bouncing from walls
	@Test
	public void simpleMoveTest0() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("d6-1-d4",true,false);
		assertGameState("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true,false,false,false);
	}
	
	@Test
	public void simpleMoveTest1() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a6-2-a4",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	@Test
	public void simpleMoveTest2() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("f6-2-f4",true,true);
		assertGameState("rr,rr,rr,rr,rr,/,,,,,/,,,,,rr/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
		assertMove("f1-2-d3",true,true);
		assertGameState("rr,rr,rr,rr,rr,/,,,,,/,,,,,rr/,,,bb,,/,,,,,/bb,bb,bb,bb,bb,",true,false,false,false);
		assertMove("b6-2-b4",true,true);
		assertGameState("rr,,rr,rr,rr,/,,,,,/,rr,,,,rr/,,,bb,,/,,,,,/bb,bb,bb,bb,bb,",false,false,false,false);
	}
	
	// capture checker tests
	@Test
	public void captureCheckerTest0() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("f6-2-f4",true,true);
		assertGameState("rr,rr,rr,rr,rr,/,,,,,/,,,,,rr/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
		assertMove("f1-2-d3",true,true);
		assertGameState("rr,rr,rr,rr,rr,/,,,,,/,,,,,rr/,,,bb,,/,,,,,/bb,bb,bb,bb,bb,",true,false,false,false);
		
	}
	
	@Test
	public void captureCheckerTest1() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a6-2-a4",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	@Test
	public void captureCheckerTest2() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a6-2-a4",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	// mirror tests
	@Test
	public void moveMirrorTest0() {
		startGame("rr,rr,rrr,rr,rr,r/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("c6-3-b3",true,true);
		assertGameState("rr,rr,,rr,rr,r/,,,,,/,,,,,/,rrr,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	@Test
	public void moveMirrorTest1() {
		startGame("rr,rr,rrr,rr,rr,r/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("c6-3-b4",true,false);
		assertMove("c6-3-b2",true,false);
		assertMove("c6-3-b3",true,true);
		assertGameState("rr,rr,,rr,rr,r/,,,,,/,,,,,/,rrr,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	@Test
	public void moveMirrorTest2() {
		startGame("rrr,rr,,,,/,,,,,/,,,,,/,rrrrrrr,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("b3-7-e6",true,true);
		assertGameState("rrr,rr,,,rrrrrrr,/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	// too-tall rule
	@Test
	public void tooTallTest0() {
		startGame("rrr,rr,,,,/,,,,,/,,,,,/,rrrrrrr,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a6-2-a4",true,false);
		assertMove("b6-1-b5",true,false);
		assertMove("b3-4-d5",true,true);
		assertGameState("rrr,rr,,,,/,,,rrrr,,/,,,,,/,rrr,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	// game simulation
	@Test
	public void gameSimTest0() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a6-2-a4",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false,false,false,false);
		assertMove("a1-1-b2",false,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,b,,,,/b,bb,bb,bb,bb,bb",true,false,false,false);
		assertMove("a4-2-c2",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,b,rr,,,/b,bb,bb,bb,bb,bb",false,false,false,false);
	}
	
	// end game test
	@Test
	public void redWonTest() {
		startGame(",,rr,rr,rr,r/,,,,,/,,,,,/,,,,,/,,,r,,/rbbb,rbbb,rbbb,bbb,,",true);
		assertMove("d2-1-d1",true,true);
		assertGameState(",,rr,rr,rr,r/,,,,,/,,,,,/,,,,,/,,,,,/rbbb,rbbb,rbbb,rbbb,,",false,true,false,true);
	}
	
	@Test
	public void blueWonTest() {
		startGame("rrr,brrr,brrr,brrr,,/b,,,,,/,,,,,/,,,,,/,,,,,/b,,b,bb,bb,bb",false);
		assertMove("a5-1-a6",true,true);
		assertGameState("brrr,brrr,brrr,brrr,,/,,,,,/,,,,,/,,,,,/,,,,,/b,,b,bb,bb,bb",true,true,false,false);
	}
	
	// player picked enemy's checker
	@Test
	public void redPickedBlueCheckerTest() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true);
		assertMove("a1-2-a3",true,false);
		assertGameState("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",true,false, false, false);
	}
	
	@Test
	public void bluePickedRedCheckerTest() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false);
		assertMove("a6-2-a4",true,false);
		assertGameState("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false ,false, false, false);
	}
	
	// player's turn but enemy has 5-height stack
	@Test
	public void redTurnBlueTooTallTest() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bbbbb,,b,bb,bb,bb",true);
		assertMove("a6-2-a4",true,true);
		assertGameState(",rr,rr,rr,rr,rr/,,,,,/rr,,,,,/,,,,,/,,,,,/bbbbb,,b,bb,bb,bb",false ,false, false, false);
	}
	
	@Test
	public void blueTurnRedTooTallTest() {
		startGame("rrrrr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bb,bb,bb,bb,bb,bb",false);
		assertMove("a1-2-a3",true,true);
		assertGameState("rrrrr,rr,rr,rr,rr,rr/,,,,,/,,,,,/bb,,,,,/,,,,,/,bb,bb,bb,bb,bb",true ,false, false, false);
	}
	
	// too-tall violated
	@Test
	public void tooTallViolatedTest() {
		startGame("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bbbbb,,b,bb,bb,bb",false);
		assertMove("c1-2-c3",true,false);
		assertGameState("rr,rr,rr,rr,rr,rr/,,,,,/,,,,,/,,,,,/,,,,,/bbbbb,,b,bb,bb,bb",false ,false, false, false);
	}
	
}
